#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Student{
    char name[51];
    char dob[11];
    char regNo[7];
    char progCode[5];
    float annualTuition;
    struct Student *next;
    struct Student *prev;
};
void create(struct Student **head);
void read(struct Student *head);
void update(struct Student *head, const char *studentdetail);
void delete(struct Student **head, const char *studentdetail);
void search(struct Student *head, char *studentdetail);
void export(struct Student *head);
void sortByName(struct Student *head);
void sortByRegNo(struct Student *head);
void freeMemory(struct Student *clonedHead);
struct Student* cloneList(struct Student *clonedHead, struct Student *head);

int main() {
    struct Student *head; //the head pointer to the start of students array

    int choice, choice2, choice3;
    char deleteDetail[51], searchDetail[51], updateDetail[51];

    //Loop throuch choices until user exists
    while(choice!=0) {
        printf("Hello, Press\n");
        printf("1: Create a new student\n");
        printf("2: Read student details\n");
        printf("3: Update student details\n");
        printf("4: Delete a new student\n");
        printf("5: Search for a student\n");
        printf("6: Export students details\n");
        printf("7: Sort students\n");
        printf("0: Exit the program\n");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                create(&head);
                break;
            case 2:
                read(head);
                break;
            case 3:
                printf("Enter Name or RegNo of student to update\n");
                scanf(" %[^\n]", updateDetail);
                update(head, updateDetail);
                break;
            case 4:
                printf("Enter Name or RegNo of student to delete\n");
                scanf(" %[^\n]", deleteDetail);
                delete(&head, deleteDetail);
                break;
            case 5:
                printf("Enter Name or regNo of student to search\n");
                scanf(" %[^\n]", searchDetail);
                search(head, searchDetail);
                break;
            case 6:
                export(head);
                break;
            case 7:
                printf("1: Sort by Name\n");
                printf("2: Sort by RegNo\n");
                scanf("%d", &choice3);
                switch (choice3) {
                    case 1:
                        sortByName(head);
                        break;
                    case 2:
                        sortByRegNo(head);
                        break;
                    default:
                        printf("Invalid choice\n");
                        break;
                }
                break;
            case 0:
                return 0;
            default:
                printf("try Again\n");
                break;

        }
    }
    return 0;
}
void create(struct Student **head){
    struct Student *student1 = malloc(sizeof(struct Student));
    if (student1 == NULL){
        printf("Memory allocation failed\n");
        return;
    }
    printf("Enter Student's name (no more than 50 characters): \n");
    scanf(" %[^\n]", student1->name);
    printf("Enter Student's dob (in YYYY-MM-DD format): \n");
    scanf(" %[^\n]", student1->dob);
    while(strlen(student1->dob) > 10) {
        printf("DOB should be at most 10 characters long: \n");
        scanf(" %[^\n]", student1->dob);
    }
    printf("Enter Student's regNo (no more than 6 characters): \n");
    scanf(" %[^\n]", student1->regNo);
    while(strlen(student1->regNo) > 6) {
        printf("RegNo should be at most 6 characters long: \n");
        scanf(" %[^\n]", student1->regNo);
    }
    printf("Enter Student's programme code (no more than 4 characters): \n");
    scanf(" %[^\n]", student1->progCode);
    while(strlen(student1->progCode) > 4) {
        printf("Program code should be at most 4 characters long: \n");
        scanf(" %[^\n]", student1->progCode);
    }
    printf("Enter Student's annual tuition: \n");
    scanf("%f", &student1->annualTuition);
    while(student1->annualTuition < 0){
        printf("Tuition should be greater than 0:\n");
        scanf("%f", &student1->annualTuition);
    }
    student1->next = NULL;
    student1->prev = NULL;

    if (*head == NULL){
        *head = student1;
    }
    else{
        struct Student *temp; //temp pointer to iterate through structure
        temp = *head; //starts from the head to the last element
        while (temp->next != NULL){
            temp = temp->next;
        }
        temp->next = student1;
        student1->prev = temp;
    }
    printf("Student created successfully!\n");
}
void read(struct Student *head) {
    if (head == NULL) {
        printf("The list is empty.\n");
    }
    else {
        struct Student *temp = head;
        int index = 1;
        printf("Index\t Name\t dob\t regNo\t programCode\t annualTuition\n");
        while (temp != NULL) {
            printf("%d\t %s\t\t\t %s\t\t %s\t %s\t\t %.2f\n", index, temp->name, temp->dob, temp->regNo, temp->progCode,
                   temp->annualTuition);
            temp = temp->next;
            index += 1;
        }
    }
}
void update(struct Student *head, const char *studentdetail) {
    if (head == NULL) {
        printf("List is empty\n");
        return;
    }
    else {
        struct Student *temp = head;
        int choice;
        while (temp != NULL) {
            if (strcmp(temp->name, studentdetail) == 0 || strcmp(temp->regNo, studentdetail) == 0) {
                printf("1: Update student's name\n");
                printf("2: Update student's DOB\n");
                printf("3: Update student's RegNO\n");
                printf("4: Update student's annual tuition\n");
                printf("5: Update student's program code\n");
                printf("6: Update all student's details\n");
                printf("0: Return to main menu\n");
                scanf("%d", &choice);
                switch (choice) {
                    case 1:
                        printf("Enter new student's name\n");
                        scanf(" %[^\n]", temp->name);
                        break;
                    case 2:
                        printf("Enter new student's DOB\n");
                        scanf(" %[^\n]", temp->dob);
                        while(strlen(temp->dob) > 10) {
                            printf("DOB should be at most 10 characters long: \n");
                            scanf(" %[^\n]", temp->dob);}
                        break;
                    case 3:
                        printf("Enter new student's RegNo\n");
                        scanf(" %[^\n]", temp->regNo);
                        while(strlen(temp->regNo) > 6) {
                            printf("RegNo should be at most 6 characters long: \n");
                            scanf(" %[^\n]", temp->regNo);}
                        break;
                    case 4:
                        printf("Enter new student's annual tuition\n");
                        scanf("%f", &temp->annualTuition);
                        while(temp->annualTuition < 0){
                            printf("Tuition should be greater than 0:\n");
                            scanf("%f", &temp->annualTuition);
                        }
                        break;
                    case 5:
                        printf("Enter new student's program code\n");
                        scanf(" %[^\n]", temp->progCode);
                        while(strlen(temp->progCode) > 4) {
                            printf("Program code should be at most 4 characters long: \n");
                            scanf(" %[^\n]", temp->progCode);
                        }
                        break;
                    case 6:
                        printf("Enter New Student's name (no more than 50 characters): \n");
                        scanf(" %[^\n]", temp->name);
                        printf("Enter New Student's dob (in YYYY-MM-DD format): \n");
                        scanf(" %[^\n]", temp->dob);
                        printf("Enter New Student's regNo (no more than 6 characters): \n");
                        scanf(" %[^\n]", temp->regNo);
                        printf("Enter New Student's programme code (no more than 4 characters: \n");
                        scanf(" %[^\n]", temp->progCode);
                        printf("Enter New Student's annual tuition: \n");
                        scanf("%f", &temp->annualTuition);
                        break;
                    case 0:
                        return;
                    default:
                        printf("Not a valid choice, try again\n");
                        break;
                }
                printf("Student's details successfully updated\n");
                return;
            }
            temp = temp->next;
        }
    }
    printf("Person not found!\n");
}

void sortByRegNo(struct Student *head) {
    if (head == NULL) {
        printf("List is empty\n");
        return;
    }

    struct Student *clonedHead = cloneList(NULL, head); // Cloning the initial list to sort it
    int sorted = 0;

    while (!sorted) {
        sorted = 1;
        struct Student *current = clonedHead;

        while (current != NULL && current->next != NULL) {
            if (strcmp(current->regNo, current->next->regNo) > 0) {
                struct Student *temp = current->next;
                current->next = temp->next;
                if (temp->next != NULL) {
                    temp->next->prev = current;
                }

                temp->next = current;
                temp->prev = current->prev;
                current->prev = temp;

                if (temp->prev != NULL) {
                    temp->prev->next = temp;
                } else {
                    clonedHead = temp;
                }

                sorted = 0;
            } else {
                current = current->next;
            }
        }
    }

    read(clonedHead);
}
void sortByName(struct Student *head) {
    if (head == NULL) {
        printf("List is empty\n");
        return;
    }

    struct Student *clonedHead = cloneList(NULL, head); // Cloning the initial list to sort it
    int sorted = 0;

    while (!sorted) {
        sorted = 1;
        struct Student *current = clonedHead;

        while (current != NULL && current->next != NULL) {
            if (strcmp(current->name, current->next->name) > 0) {
                struct Student *temp = current->next;
                current->next = temp->next;
                if (temp->next != NULL) {
                    temp->next->prev = current;
                }

                temp->next = current;
                temp->prev = current->prev;
                current->prev = temp;

                if (temp->prev != NULL) {
                    temp->prev->next = temp;
                } else {
                    clonedHead = temp;
                }

                sorted = 0;
            } else {
                current = current->next;
            }
        }
    }

    read(clonedHead);
}

void freeMemory(struct Student *clonedHead){
    //Cleaning up the memory of the entire list
    while (clonedHead != NULL) {
        struct Student *next = clonedHead->next;
        free(clonedHead);
        clonedHead = next;
    }
}

struct Student* cloneList(struct Student *clonedHead, struct Student *head){
    struct Student *clonedTail = NULL;
    struct Student *temp = head;
    struct Student *prev_element = head;
    while (temp != NULL){
        struct Student *student = malloc(sizeof(struct Student));
        if (student == NULL) {
            printf("Memory allocation failed\n");
            freeMemory(clonedHead); //free memory if allocation fails
            return NULL;
        }

        strcpy(student->name, temp->name);
        strcpy(student->dob, temp->dob);
        strcpy(student->regNo, temp->regNo);
        strcpy(student->progCode, temp->progCode);
        student->annualTuition = temp->annualTuition;
        student->next = NULL;
        if (temp->prev == NULL){
            clonedHead = student; //clonedHead now holds the first clone of the list
            student->prev = NULL;
        }
        else{
            student->prev = prev_element;
            clonedTail->next = student;
            prev_element = student;
        }
        temp = temp->next;
        clonedTail = student;
    }
    return clonedHead;
}
