# Student Management System in C

This is a group project for a Student Management System implemented in C. The system allows users to create, read, update, delete, search, and export student records.

## Functionality Overview

- **Create**: Allows the user to add a new student to the system.
- **Read**: Displays all student records in the system.
- **Update**: Allows the user to modify existing student records.
- **Delete**: Allows the user to remove a student from the system.
- **Search**: Allows the user to find a student by name, date of birth, registration number, or program code.
- **Export**: Writes all student records to a CSV file for external use.

## Why Structures?

We have chosen to use structures in our project because they improve the readability of the code and provide a clean and organized way to manage student data. Using structures also allows for easy extension and modification of the system in the future. If additional information needs to be added to student records, we can simply add attributes to the structure.

## Sorting and Searching

Due to the limitations of the C language and its linked lists, we have implemented the bubble sort algorithm for sorting student records. Similarly, we have used a linear search algorithm for searching student records. While these algorithms may not be the most efficient for large datasets, they serve our purposes well in this project.

## YouTube Video Walkthroughs

- [Create, Read, and Update functions](https://youtu.be/iHjgZg4N3d0)
- [Delete, Search, and Export functions](https://youtu.be/ALbrAx8uO5U)
- [sort by name, sort by regno functions](https://youtu.be/rZLnbLzWoMo)